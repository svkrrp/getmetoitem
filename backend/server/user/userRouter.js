const express = require('express');

const router = new express.Router();
const geolib = require('geolib');
const User = require('../models/user');
const Seller = require('../models/seller');
const auth = require('../auth/userAuth');

router.get('/', (req, res) => {
  res.render('index');
});

router.post('/user/signup', async (req, res) => {
  try {
    const user = await new User(req.body).save();
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (error) {
    res.send(error);
  }
});

router.post('/user/login', async (req, res) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
      password: req.body.password,
    });
    if (!user) {
      throw new Error('Invalid Credentials');
    }
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (error) {
    res.send(error);
  }
});

router.post('/home', auth, async (req, res) => {
  try {
    const shopArray = [];
    const sellers = await Seller.find({});
    if (!sellers) {
      throw new Error('sellers not found');
    }
    sellers.forEach((seller) => {
      const isItemPresent = seller.items.filter(
        (item) => item.name === req.body.itemName,
      ).length;

      if (isItemPresent) {
        const pos = {
          latitude: seller.latitude,
          longitude: seller.longitude,
        };
        const obj = {
          shopName: seller.shopName,
          shopAddress: seller.shopAddress,
          distance: geolib.getDistance(pos, {
            latitude: req.body.latitude,
            longitude: req.body.longitude,
          }),
        };
        shopArray.push(obj);
      }
    });
    res.send(shopArray);
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
