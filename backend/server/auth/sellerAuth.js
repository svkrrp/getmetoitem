/* eslint-disable no-underscore-dangle */
const jwt = require('jsonwebtoken');
const Seller = require('../models/seller');

const auth = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = jwt.verify(token, process.env.AUTH_KEY);

    const seller = await Seller.findOne({
      _id: decoded._id,
      'tokens.token': token,
    });

    if (!seller) {
      throw new Error();
    }

    req.seller = seller;
    next();
  } catch (error) {
    res.send(error);
  }
};

module.exports = auth;
