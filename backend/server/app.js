const express = require('express');

const app = express();
require('./db/dbConfig');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const hbs = require('hbs');
const userRouter = require('./user/userRouter');
const sellerRouter = require('./seller/sellerRouter');

const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partailsPath = path.join(__dirname, '../templates/partials');

hbs.registerPartials(partailsPath);

app.use(cors());
app.use(express.static(publicDirectoryPath));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.json());

app.set('views', viewsPath);
app.set('view engine', 'hbs');

app.use(userRouter);
app.use(sellerRouter);

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
