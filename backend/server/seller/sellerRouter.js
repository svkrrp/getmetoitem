const express = require('express');

const router = new express.Router();
const NodeGeocoder = require('node-geocoder');
const Seller = require('../models/seller');
const auth = require('../auth/sellerAuth');

const options = {
  provider: 'here',
  apiKey: 'PjFmJ8trenSAooNCCUPagf7ifPsyZK-RGHlxmkBvQhw',
  formatter: null,
};

const geocoder = NodeGeocoder(options);

router.post('/seller/signup', async (req, res) => {
  try {
    const { shopAddress } = req.body;
    const location = await geocoder.geocode(shopAddress.toString());

    if (!location.formattedAddress) {
      throw new Error('Invalid Address');
    }

    const userObject = {
      ...req.body,
      shopAddress: location[0].formattedAddress,
      latitude: location[0].latitude,
      longitude: location[0].longitude,
    };
    const seller = await new Seller(userObject).save();
    if (!seller) {
      throw new Error('Network Error');
    }
    const token = await seller.generateAuthToken();
    res.send({ seller, token });
  } catch (error) {
    res.send(`Error: ${error}`);
  }
});

router.post('/seller/login', async (req, res) => {
  try {
    const seller = await Seller.findOne({
      username: req.body.username,
      password: req.body.password,
    });
    if (!seller) {
      throw new Error('User not found');
    }
    const token = await seller.generateAuthToken();
    res.send({ seller, token });
  } catch (error) {
    res.send(`Error: ${error}`);
  }
});

router.get('/seller/items', auth, (req, res) => {
  try {
    console.log(req.seller.items);
    res.send(req.seller.items);
  } catch (error) {
    res.send('Invalid User');
  }
});

router.post('/seller/home', auth, async (req, res) => {
  try {
    req.seller.items = req.seller.items.concat({
      name: req.body.name,
      price: req.body.price,
    });
    await req.seller.save();
    res.send({
      name: req.body.name,
      price: req.body.price,
    });
  } catch (error) {
    res.send('Invalid User');
  }
});

router.post('/seller/item/:name', auth, async (req, res) => {
  try {
    req.seller.items = req.seller.items.filter((item) => item.name !== req.params.name);
    await req.seller.save();
    res.send('deleted');
  } catch (error) {
    res.send('error');
  }
});

module.exports = router;
