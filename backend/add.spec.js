/* eslint-disable no-undef */
const add = require('./add');

describe('add', () => {
  it('add test', () => {
    expect(add(2, 3)).toBe(5);
  });
});
