/* eslint-disable no-new */
/* eslint-disable import/extensions */
import { Login } from './modules/login.js';

class App {
  static run() {
    new Login();
  }
}

App.run();
