/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */
export class SellerHome {
  constructor(token) {
    this.token = token;
    this.render();
  }

  async createItemListTemplate() {
    try {
      const response = await axios.get('/seller/items', {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      });
      const items = response.data;
      if (items.length) {
        const itemsEl = document.createElement('div');
        itemsEl.className = 'shop-list';
        items.forEach((item) => {
          const itemEl = this.createShopElement(item);
          itemsEl.appendChild(itemEl);
        });
        if (
          document.getElementById('app').querySelector('.shop-list')
        ) {
          const delEl = document
            .getElementById('app')
            .querySelector('.shop-list');
          document.getElementById('app').removeChild(delEl);
        }
        document.getElementById('app').appendChild(itemsEl);
      }
    } catch (error) {
      console.log('Invalid User');
    }
  }

  createShopElement(item) {
    const itemEl = document.createElement('div');
    itemEl.className = 'shop';
    itemEl.innerHTML = `
      <div class="shop-name">Item Name: ${item.name}</div>
      <div class="shop-address">Item Price: ${item.price}</div>
      `;
    return itemEl;
  }

  createFormEl() {
    const formEl = document.createElement('div');
    formEl.innerHTML = `
        <div class="seller-home">
          <div class="item-name">
            <input type="text" name="item-name" id="item-name" placeholder="item name">
          </div>
          <div class="item-price">
            <input type="text" name="item-price" id="item-price" placeholder="item price">
          </div>
          <button class="add">ADD</button>
        </div>
        `;
    const addButton = formEl.querySelector('.add');
    addButton.addEventListener('click', async () => {
      const itemName = formEl.querySelector('#item-name');
      const itemPrice = formEl.querySelector('#item-price');

      if (!itemName.value || !itemPrice.value) {
        alert('fill both fields');
        return;
      }
      const name = itemName.value;
      const price = itemPrice.value;
      try {
        const response = await axios.post(
          '/seller/home',
          {
            name,
            price,
          },
          {
            headers: {
              Authorization: `Bearer ${this.token}`,
            },
          },
        );
        itemName.value = '';
        itemPrice.value = '';
        console.log(response);
        this.createItemListTemplate();
      } catch (error) {
        console.log('invalid user');
      }
    });
    return formEl;
  }

  render() {
    const formEl = this.createFormEl();

    if (document.querySelector('main')) {
      document
        .getElementById('app')
        .removeChild(document.querySelector('main'));
    }
    document.getElementById('app').appendChild(formEl);
    this.createItemListTemplate();
  }
}
