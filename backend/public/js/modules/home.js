/* eslint-disable comma-dangle */
/* eslint-disable operator-linebreak */
/* eslint-disable no-undef */
/* eslint-disable import/extensions */
/* eslint-disable import/no-cycle */
/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
export class Home {
  constructor(token) {
    this.token = token;
    this.render();
  }

  async getNearestShops(crd, itemName) {
    try {
      const response = await axios.post(
        '/home',
        {
          latitude: crd.latitude,
          longitude: crd.longitude,
          itemName,
        },
        {
          headers: {
            Authorization: `Bearer ${this.token}`,
          },
        }
      );
      console.log(response.data);
      return response.data;
    } catch (error) {
      alert('Invalid user');
      return error;
    }
  }

  createShopElement(shop) {
    const shopEl = document.createElement('div');
    shopEl.className = 'shop';
    shopEl.innerHTML = `
        <div class="shop-name">Shop Name: ${shop.shopName}</div>
        <div class="shop-address">Shop Address: ${shop.shopAddress}</div>
        <div class="shop-distance">Distance: ${shop.distance} meters</div>
        `;
    return shopEl;
  }

  createFormEl() {
    const formEl = document.createElement('div');
    formEl.innerHTML = `
    <div class="seller-home">
      <div class="item-name">
        <input type="text" name="item-name" id="item-name" placeholder="item name">
      </div>
      <button class="search">Search</button>
    </div>
    `;
    const searchBtn = formEl.querySelector('.search');
    searchBtn.addEventListener('click', () => {
      const itemName = formEl.querySelector('#item-name').value;
      if (!itemName) {
        alert('enter item to search');
        return;
      }
      if (!navigator.geolocation) {
        alert('Your browser does not support geolocation');
        return;
      }
      navigator.geolocation.getCurrentPosition(
        async (pos) => {
          const shops = await this.getNearestShops(pos.coords, itemName);
          if (shops.length) {
            const shopsEl = document.createElement('div');
            shopsEl.className = 'shop-list';
            shops.forEach((shop) => {
              const shopEl = this.createShopElement(shop);
              shopsEl.appendChild(shopEl);
            });
            if (document.getElementById('app').querySelector('.shop-list')) {
              const delEl = document
                .getElementById('app')
                .querySelector('.shop-list');
              document.getElementById('app').removeChild(delEl);
            }
            document.getElementById('app').appendChild(shopsEl);
            document.getElementById('app').querySelector('#item-name').value =
              '';
          } else {
            alert('no shops found');
          }
        },
        (err) => {
          console.warn(`ERROR(${err.code}): ${err.message}`);
        },
        {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0,
        }
      );
    });
    return formEl;
  }

  render() {
    const formEl = this.createFormEl();
    if (document.querySelector('main')) {
      document
        .getElementById('app')
        .removeChild(document.querySelector('main'));
    }
    document.getElementById('app').appendChild(formEl);
  }
}
