/* eslint-disable operator-linebreak */
/* eslint-disable no-undef */
/* eslint-disable import/extensions */
/* eslint-disable import/no-cycle */
/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
import { SellerSignup } from './sellerSignup.js';
import { Signup } from './signup.js';
import { SellerHome } from './sellerHome.js';

export class SellerLogin {
  constructor() {
    this.render();
  }

  getLoginTemplate() {
    const main = document.createElement('main');
    main.innerHTML = `
    <div class="login-form">
      <form>
        <div class="username">
          <input type="text" name="username" id="username" placeholder="username" />
        </div>
        <div class="password">
          <input type="password" name="password" id="password" placeholder="password" />
        </div>
        <div class="submit-button">
          <button>LOGIN</button>
        </div>
        <div class="create-new">
          <p>
            Not a shop keeper?
            <a class="signup">User Sign Up</a>
          </p>
        </div>
        <div class="create-new">
          <p>
            Not registered?
            <a class="seller-signup">Seller SignUp</a>
          </p>
        </div>
      </form>
    </div>
    `;

    const sellerSignup = main.querySelector('.seller-signup');
    sellerSignup.addEventListener('click', () => {
      new SellerSignup();
    });

    const signup = main.querySelector('.signup');
    signup.addEventListener('click', () => {
      new Signup();
    });

    const submitBtn = main.querySelector('.submit-button button');
    submitBtn.addEventListener('click', async (event) => {
      event.preventDefault();
      const username = main.querySelector('#username');
      const password = main.querySelector('#password');
      if (!username.value || !password.value) {
        alert('Fill all details');
        return;
      }
      try {
        const response = await axios.post('/seller/login', {
          username: username.value,
          password: password.value,
        });
        new SellerHome(response.data.token);
      } catch (error) {
        alert('Invalid credentials');
      }
    });

    return main;
  }

  render() {
    const main = this.getLoginTemplate();
    if (document.querySelector('main')) {
      document
        .getElementById('app')
        .removeChild(document.querySelector('main'));
    }
    document.getElementById('app').appendChild(main);
  }
}
