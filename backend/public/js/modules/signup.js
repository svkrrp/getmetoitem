/* eslint-disable comma-dangle */
/* eslint-disable operator-linebreak */
/* eslint-disable no-undef */
/* eslint-disable import/extensions */
/* eslint-disable import/no-cycle */
/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
import { SellerSignup } from './sellerSignup.js';
import { Login } from './login.js';
import { Home } from './home.js';

export class Signup {
  constructor() {
    this.render();
  }

  getSignupTemplate() {
    const main = document.createElement('main');
    main.innerHTML = `
    <div class="login-form">
      <form>
        <div class="username">
          <input type="text" name="username" id="username" placeholder="username" />
        </div>
        <div class="email">
          <input type="text" name="email" id="email" placeholder="email" />
        </div>
        <div class="password">
          <input type="password" name="password" id="password" placeholder="password" />
        </div>
        <div class="submit-button">
          <button>SIGN UP</button>
        </div>
        <div class="create-new">
          <p>
            Already registered
            <a class="login">User Login</a>
          </p>
        </div>
        <div class="create-new">
          <p>
            Are you a Shop keeper?
            <a class="seller-signup">Seller Sign Up</a>
          </p>
        </div>
      </form>
    </div>
    `;

    const login = main.querySelector('.login');
    login.addEventListener('click', () => {
      new Login();
    });

    const sellerSignup = main.querySelector('.seller-signup');
    sellerSignup.addEventListener('click', () => {
      new SellerSignup();
    });

    const submitBtn = main.querySelector('.submit-button button');
    submitBtn.addEventListener('click', async (event) => {
      event.preventDefault();
      const username = main.querySelector('#username');
      const password = main.querySelector('#password');
      const email = main.querySelector('#email');
      if (!username.value || !password.value || !email.value) {
        alert('Fill all details');
        return;
      }
      try {
        const response = await axios.post('/signup', {
          username: username.value,
          email: email.value,
          password: password.value,
        });
        new Home(response.data.token);
      } catch (error) {
        alert('Server Error: PLease try again later');
      }
    });

    return main;
  }

  render() {
    const main = this.getSignupTemplate();
    if (document.querySelector('main')) {
      document
        .getElementById('app')
        .removeChild(document.querySelector('main'));
    }
    document.getElementById('app').appendChild(main);
  }
}
