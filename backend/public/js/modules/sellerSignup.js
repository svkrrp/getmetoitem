/* eslint-disable operator-linebreak */
/* eslint-disable no-undef */
/* eslint-disable import/extensions */
/* eslint-disable import/no-cycle */
/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
import { Signup } from './signup.js';
import { SellerLogin } from './sellerLogin.js';
import { SellerHome } from './sellerHome.js';

export class SellerSignup {
  constructor() {
    this.render();
  }

  getLoginTemplate() {
    const main = document.createElement('main');
    main.innerHTML = `
    <div class="login-form">
      <form>
        <div class="username">
          <input type="text" name="username" id="username" placeholder="username" />
        </div>
        <div class="email">
          <input type="text" name="email" id="email" placeholder="email" />
        </div>

        <div class="password">
          <input type="password" name="password" id="password" placeholder="password" />
        </div>
        <div class="shop_name">
          <input type="text" name="shop_name" id="shop_name" placeholder="shop name" />
        </div>
        <div class="shop_address">
          <input type="text" name="shop_address" id="shop_address" placeholder="shop address" />
        </div>
        <div class="submit-button">
          <button>SIGN UP</button>
        </div>
        <div class="create-new">
          <p>
            Already registerd?
            <a class="seller-login">Seller Login</a>
          </p>
        </div>
        <div class="create-new">
          <p>
            Not a shop keeper?
            <a class="signup">User Sign Up</a>
          </p>
        </div>
      </form>
    </div>
    `;

    const signup = main.querySelector('.signup');
    signup.addEventListener('click', () => {
      new Signup();
    });

    const sellerLogin = main.querySelector('.seller-login');
    sellerLogin.addEventListener('click', () => {
      new SellerLogin();
    });

    const submitBtn = main.querySelector('.submit-button button');
    submitBtn.addEventListener('click', async (event) => {
      event.preventDefault();
      const username = main.querySelector('#username');
      const password = main.querySelector('#password');
      const shopName = main.querySelector('#shop_name');
      const shopAddress = main.querySelector('#shop_address');
      const email = main.querySelector('#email');
      if (
        !username.value ||
        !email.value ||
        !password.value ||
        !shopName.value ||
        !shopAddress.value
      ) {
        alert('fill all the fields');
        return;
      }

      try {
        const response = await axios.post('/seller/signup', {
          username: username.value,
          email: email.value,
          password: password.value,
          shopName: shopName.value,
          shopAddress: shopAddress.value,
        });
        console.log(response);
        if (response.data.includes('Error')) {
          throw new Error('Invalid Credentials');
        }
        new SellerHome(response.data.token);
      } catch (error) {
        alert('Invalid Credentials');
      }
    });

    return main;
  }

  render() {
    const main = this.getLoginTemplate();
    if (document.querySelector('main')) {
      document
        .getElementById('app')
        .removeChild(document.querySelector('main'));
    }
    document.getElementById('app').appendChild(main);
  }
}
