# Project

> Full application using Vue.js as frontend and node as backend and mongo as database

## Build Setup

``` bash
# clone project
git clone https://gitlab.com/svkrrp/getmetoitem.git

# run docker compose
docker-compose up
```