/* eslint-disable eol-last */
/* eslint-disable import/prefer-default-export */
import UserLogin from '../screens/UserLogin.vue';
import SellerLogin from '../screens/SellerLogin.vue';
import UserSignup from '../screens/UserSignup.vue';
import SellerSignup from '../screens/SellerSignup.vue';
import UserHome from '../screens/UserHome.vue';
import SellerHome from '../screens/SellerHome.vue';

export const routes = [
  { path: '', component: UserLogin },
  { path: '/signup', component: UserSignup },
  { path: '/sellerLogin', component: SellerLogin },
  { path: '/sellerSignup', component: SellerSignup },
  { path: '/userHome', component: UserHome },
  { path: '/sellerHome', component: SellerHome },
];