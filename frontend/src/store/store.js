/* eslint-disable eol-last */
/* eslint-disable import/prefer-default-export */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    count: 0,
    shopArray: [],
    itemArray: [],
    loading: false,
    token: '',
    name: ''
  },
});