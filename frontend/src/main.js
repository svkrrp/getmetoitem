import Vue from "vue";
import VueRouter from 'vue-router';
import VueI18n from 'vue-i18n'
import App from "./App.vue";
import { store } from './store/store.js';
import { routes } from "./router/routes.js";
import messages from './locales/messages';

Vue.use(VueRouter);
Vue.use(VueI18n);

// routing
const router = new VueRouter({
  routes,
  mode: 'history',
});

// i18n support
const i18n = new VueI18n({
  locale: 'en', // set locale
  messages, // set locale messages
})

new Vue({
  el: "#app",
  store,
  router,
  i18n,
  render: h => h(App)
});
